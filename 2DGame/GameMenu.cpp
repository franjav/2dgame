#include "GameMenu.h"
#include "Texture.h"


// default constructor
GameMenu::GameMenu()
{
	textMenu = 0;
	current_option = -1;
	nOptions = 4;
	useSound = true;
	bgID = 0;
}

// mouse move call back (with click)
void GameMenu::mouseMove(int x, int y)
{

	
}

// mouse move call back (without click)
void GameMenu::mousePassiveMove(int x, int y)
{
	int viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);
	x = 100 * x / viewport[2];
	y = 100 * y / viewport[3];


	// checking which option is under the mouse pointer
	if (x < 20 || x>80 || y < 20 || y> 80)
		current_option = -1;
	else
		current_option = (5 * (y - 20) / 60) % 5;
}

int GameMenu::mouseEvent(int button, int state, int x, int y)
{
	// studying click event
	if ((button == GLUT_LEFT_BUTTON) && (state == GLUT_UP))
	{
		int viewport[4];
		glGetIntegerv(GL_VIEWPORT, viewport);
		x = 100 * x / viewport[2];
		y = 100 * y / viewport[3];

		// checking outside the menu
		if (x < 20 || x>80 || y < 20 || y> 80)
			current_option = -1;
		else
		{
			// determining the current option
			current_option = (5 * (y - 20) / 60) % 5;

			// playing a sound
			soundMgr->playAudio(clickID, true);
			if (current_option == 1)
				useSound = !useSound;
		}
		return current_option;
	}
	return - 1;
}

// display function
void GameMenu::display()
{
	// setting projection to 100x100 virtual window
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, 100, 0, 100,-1,1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
			
	glColor4f(1, 1, 1, 1);
	glEnable(GL_TEXTURE_2D);

	// background texture
	glBindTexture(GL_TEXTURE_2D, bgID);
	glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex3f(0, 0, 0.0f);

		glTexCoord2f(1, 0);
		glVertex3f(100, 0, 0.0f);

		glTexCoord2f(1, 1);
		glVertex3f(100, 100, 0.0f);

		glTexCoord2f(0, 1);
		glVertex3f(0, 100, 0.0f);
	glEnd();

	// menu texture
	glBindTexture(GL_TEXTURE_2D, textMenu);

	float t0, t1;
	float y;
	int i;
	//current_option = 0;
	float h = 0.0f;
	y = 80;
	t0 = (current_option != 0) ? 0.0f  : 0.5f;
	t1 = (current_option != 0) ? 0.5f : 1.0f;
	float itemWidth = 1.0f / 6.0f;

	// displaying option 0
	glBegin(GL_QUADS);
		glTexCoord2f(0, t0);
		glVertex3f(20, y, 0.0f);

		glTexCoord2f(itemWidth, t0);
		glVertex3f(80, y, 0.0f);

		glTexCoord2f(itemWidth, t1);
		glVertex3f(80, y-60.0f/5.0f, 0.0f);

		glTexCoord2f(0, t1);
		glVertex3f(20, y - 60.0f / 5.0f, 0.0f);
	glEnd();

	y = 80 - 60.0f / 5.0f;
	i = (useSound) ? 1 : 2;
	t0 = (current_option != 1) ? 0.0f : 0.5f;
	t1 = (current_option != 1) ? 0.5f : 1.0f;

	// displaying option 1
	glBegin(GL_QUADS);
		glTexCoord2f(i*itemWidth, t0);
		glVertex3f(20, y, 0.0f);

		glTexCoord2f((i+1)*itemWidth, t0);
		glVertex3f(80, y, 0.0f);

		glTexCoord2f((i + 1)*itemWidth, t1);
		glVertex3f(80, y - 60.0f / 5.0f, 0.0f);

		glTexCoord2f(i*itemWidth, t1);
		glVertex3f(20, y - 60.0f / 5.0f, 0.0f);
	glEnd();

	y = 80 - 2*60.0f / 5.0f;
	i = 3;
	t0 = (current_option != 2) ? 0.0f : 0.5f;
	t1 = (current_option != 2) ? 0.5f : 1.0f;
	// displaying option 2
	glBegin(GL_QUADS);
		glTexCoord2f(i*itemWidth, t0);
		glVertex3f(20, y, 0.0f);

		glTexCoord2f((i+1)*itemWidth, t0);
		glVertex3f(80, y, 0.0f);

		glTexCoord2f((i+1)*itemWidth, t1);
		glVertex3f(80, y - 60.0f / 5.0f, 0.0f);

		glTexCoord2f(i*itemWidth, t1);
		glVertex3f(20, y - 60.0f / 5.0f, 0.0f);
	glEnd();

	y = 80 - 3 * 60.0f / 5.0f;
	i = 4;
	t0 = (current_option != 3) ? 0.0f : 0.5f;
	t1 = (current_option != 3) ? 0.5f : 1.0f;
	// displaying option 3
	glBegin(GL_QUADS);
		glTexCoord2f(i*itemWidth, t0);
		glVertex3f(20, y, 0.0f);

		glTexCoord2f((i+1)*itemWidth, t0);
		glVertex3f(80, y, 0.0f);

		glTexCoord2f((i + 1)*itemWidth, t1);
		glVertex3f(80, y - 60.0f / 5.0f, 0.0f);

		glTexCoord2f(i*itemWidth, t1);
		glVertex3f(20, y - 60.0f / 5.0f, 0.0f);
	glEnd();

	y = 80 - 4 * 60.0f / 5.0f;
	i = 5;
	t0 = (current_option != 4) ? 0.0f : 0.5f;
	t1 = (current_option != 4) ? 0.5f : 1.0f;
	// displaying option 4
	glBegin(GL_QUADS);
		glTexCoord2f(i*itemWidth, t0);
		glVertex3f(20, y, 0.0f);

		glTexCoord2f((i + 1)*itemWidth, t0);
		glVertex3f(80, y, 0.0f);

		glTexCoord2f((i + 1)*itemWidth, t1);
		glVertex3f(80, y - 60.0f / 5.0f, 0.0f);

		glTexCoord2f(i*itemWidth, t1);
		glVertex3f(20, y - 60.0f / 5.0f, 0.0f);
	glEnd();

	glDisable(GL_TEXTURE_2D);
}

// initializing menu
bool GameMenu::Init()
{
	// loading audio
	soundMgr = SoundManager::createManager();
	soundMgr->loadAudio(".\\Sound\\Click.ogg", &clickID, false);

	// loading textures
	textMenu = Texture::loadTexture("textures\\menu1024x64x24.raw", 1024, 64, 3);
	if (textMenu == 0)
		return false;
	bgID = Texture::loadTexture("textures\\background.raw", 1974, 406, 4);
	if (bgID == 0)
		return false;

	return true;
}

// destroy method
GameMenu::~GameMenu()
{

	// Force an audio source to stop playing.
	soundMgr->stopAudio(clickID);

	//Release audio
	soundMgr->releaseAudio(clickID);
}

// return true if sound is ON
bool GameMenu::isSoundOn(){
	return useSound;
}