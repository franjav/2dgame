#pragma once

#include "definitions.h"
#include <stdio.h>
#include <stdlib.h>

#include "SoundManager.h"

// game menu class
class GameMenu
{
public:

	// default constructor
	GameMenu();

	// destructor
	~GameMenu();

	// initialize all the menu stuff
	bool Init();

	// display the menu
	void display();

	// mouse move callback (with click)
	void mouseMove(int x, int y);

	// mouse move callback (without call back)
	void mousePassiveMove(int x, int y);

	// mouse click event callback
	int mouseEvent(int button, int state, int x, int y);
	
	// return true if sound is ON
	bool isSoundOn();

private:
	// texture object for the menu
	GLuint textMenu;

	// current menu option (between -1 and 5)
	int current_option;

	// number of options
	int nOptions;

	// bool indicating if we are using sound or not
	bool useSound;

	// last mouse position
	int m_lastX, m_lastY;

	// pointer to sound manager
	SoundManager *soundMgr; //Sound manager
	
	// id of the sound used when we click on menu
	unsigned int clickID; //Click ID sound

	// background texture id (sky background)
	GLuint bgID;
};

