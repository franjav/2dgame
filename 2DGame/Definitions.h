#pragma comment (lib,"./lib/freeglut.lib")
#pragma comment (lib,"./lib/libogg.lib")
#pragma comment (lib,"./lib/libvorbis.lib")
#pragma comment (lib,"./lib/libvorbisfile.lib")
#pragma comment (lib,"./lib/OpenAL32.lib")
#pragma comment (lib, "opengl32.lib")


#include "include\AL\al.h"
#include "include\AL\alc.h"
#include "include\GL\freeglut.h"
#include "include/glm/glm.hpp"
#include "include/glm/gtc/matrix_transform.hpp"
#include "include/glm/gtc/type_ptr.hpp"
#include "include/glm/gtx/quaternion.hpp"	
#include <stdlib.h>
#include <stdio.h>

#include <iostream>


//Some constants for the player and enemy moving
#define GRAVITY						0.05f
#define CHAR_VELOCITY				0.15f
#define CHAR_JUMP_ACC				0.5f
#define ENEMY_VELOCITY				0.07f
#define ENEMY_JUMP_ACC				0.4f


//Max with and max height of the map. Used in Level editor
#define MAX_LVL_WIDTH				500
#define MAX_LVL_HEIGHT				 20



using std::cout;
using std::endl;
