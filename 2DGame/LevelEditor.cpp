#include "LevelEditor.h"
#include "Texture.h"
#include <algorithm>

//level editor constructor
LevelEditor::LevelEditor(){

	//with and height of the map
	MapWidth = MAX_LVL_WIDTH;
	MapHeight = MAX_LVL_HEIGHT;

	//Size of the window. Always 20x20
	WindowWidth = 20;
	WinowHeigth = 20;

	//Half window size
	HalfWindowWidth = WindowWidth / 2;
	HalfWindowHeight = WinowHeigth / 2;

	windowPos = glm::vec2(HalfWindowWidth,10);
	mousePos = glm::vec2(0,0);
	

	//Load textures
	texID = Texture::loadTexture("textures\\map_elements.raw", 1024, 64, 4);
	characterID = Texture::loadTexture("textures\\player.raw", 512, 64, 4);
	bgID = Texture::loadTexture("textures\\background.raw", 1974, 406, 4);
	enemyTexID = Texture::loadTexture("textures\\bird.raw", 1024, 64, 4);
	
	//function to init some components
	Init();

}

// default destructor
LevelEditor::~LevelEditor(){
	
			
}

//Init of the class
void LevelEditor::Init(){
	leftButtonPress = false;
	controlPress = false;
	actual = 'x';

	//no player and final set by now
	finalPos = glm::ivec2(-1, -1);
	playerPos = glm::ivec2(-1, -1);

	//set the map to blank
	for (int i = 0; i < MapHeight; i++)
		for (int j = 0; j < MapWidth; j++)
			scene[i][j] = ' ';
}

//Function to caltulate the current viewport depending on the player position
glm::vec2 LevelEditor::calculateViewportPosition(glm::vec2 playerPos){
	glm::vec2 pos = playerPos;

	if (playerPos.x - (HalfWindowWidth) <= 0.0f ) pos.x = (float)HalfWindowWidth;
	if (playerPos.x + HalfWindowWidth >= MapWidth) pos.x = float(MapWidth - HalfWindowWidth);

	return pos;
}

//Draw the background
void LevelEditor::DrawBG(){

	//Draw bg in the screen
	int viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);

	//the map is going to scroll
	float s0 = 0.0f;
	float s1 = ((MAX_LVL_WIDTH / 20.0f) * viewport[2]) / 1971.0f;

	float t0 = 0.0f;
	float t1 = 1.0f;

	glBegin(GL_QUADS);
	glTexCoord2f(s0, t1);
	glVertex3i(0, 0, 0);
	glTexCoord2f(s1, t1);
	glVertex3i(MAX_LVL_WIDTH, 0, 0);
	glTexCoord2f(s1, t0);
	glVertex3i(MAX_LVL_WIDTH, MAX_LVL_HEIGHT, 0);
	glTexCoord2f(s0, t0);
	glVertex3i(0, MAX_LVL_HEIGHT, 0);
	glEnd();
}

//Function to draw and object in the coordiantes i, j of the screen, depending of the char
void LevelEditor::DrawChar(int i, int j, char c){
	
	if (c == 'x' || c == 'y' || c == 'z'){
		//Draw solid
		float s0 = (int)(c - 'x') * 64.0f / 1024.0f;
		float s1 = (int)(c - 'x' + 1) * 64.0f / 1024.0f;
		// we draw a box
		glBegin(GL_QUADS);
		glTexCoord2f(s0, 0);
		glVertex3i(j, MapHeight - i, 0);
		glTexCoord2f(s1, 0);
		glVertex3i(j + 1, MapHeight - i, 0);
		glTexCoord2f(s1, 1);
		glVertex3i(j + 1, MapHeight - i - 1, 0);
		glTexCoord2f(s0, 1);
		glVertex3i(j, MapHeight - i - 1, 0);
		glEnd();
	}
	else if (c == '1' || c == '2' || c == '3'){
		//Draw the enemy
		//Only draw initial frame
		glBindTexture(GL_TEXTURE_2D, enemyTexID);

		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex3i(j, MapHeight - i, 0);

		glTexCoord2f(1.0f / 14.0f, 0);
		glVertex3i(j + 1, MapHeight - i, 0);

		glTexCoord2f(1.0f / 14.0f, 1);
		glVertex3i(j + 1, MapHeight - i - 1, 0);

		glTexCoord2f(0, 1);
		glVertex3i(j, MapHeight - i - 1, 0);
		glEnd();

		glBindTexture(GL_TEXTURE_2D, texID);
	}
	else if (c == 'c'){
		//Draw the coins

		//Only draw initial frame
		float s0 = FIRST_COIN_ANIMATION_IMAGE * 64.0f / 1024.0f;
		float s1 = (FIRST_COIN_ANIMATION_IMAGE + 1) * 64.0f / 1024.0f;

		glBegin(GL_QUADS);
		glTexCoord2d(s0, 0);
		glVertex3i(j, MapHeight - i, 0);
		glTexCoord2d(s1, 0);
		glVertex3i(j + 1, MapHeight - i, 0);
		glTexCoord2d(s1, 1);
		glVertex3i(j + 1, MapHeight - i - 1, 0);
		glTexCoord2d(s0, 1);
		glVertex3i(j, MapHeight - i - 1, 0);
		glEnd();
	}
	else if (c == 'p'){
		//Draw the player with its corresponding texture
		glBindTexture(GL_TEXTURE_2D, characterID);

		//Only draw initial frame
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0); 
		glVertex3i(j, MapHeight - i, 0);

		glTexCoord2f(1.0f / 11.0f, 0);
		glVertex3i(j + 1, MapHeight - i, 0);

		glTexCoord2f(1.0f / 11.0f, 1);
		glVertex3i(j + 1, MapHeight - i - 1, 0);

		glTexCoord2f(0, 1); 
		glVertex3i(j, MapHeight - i - 1, 0);
		glEnd();

		glBindTexture(GL_TEXTURE_2D, texID);

	}
	else if (c == 'f')
	{
		//draw final 
		float s0 = 640.0f / 1024.0f;
		float s1 = 704.0f / 1024.0f;
		// we draw a box
		glBegin(GL_QUADS);
		glTexCoord2f(s0, 0);
		glVertex3i(j, MapHeight - i, 0);
		glTexCoord2f(s1, 0);
		glVertex3i(j + 1, MapHeight - i, 0);
		glTexCoord2f(s1, 1);
		glVertex3i(j + 1, MapHeight - i - 1, 0);
		glTexCoord2f(s0, 1);
		glVertex3i(j, MapHeight - i - 1, 0);

		s0 = 704.0f / 1024.0f;
		s1 = 768.0f / 1024.0f;

		glTexCoord2f(s0, 0);
		glVertex3i(j, MapHeight - i + 1, 0);
		glTexCoord2f(s1, 0);
		glVertex3i(j + 1, MapHeight - i + 1, 0);
		glTexCoord2f(s1, 1);
		glVertex3i(j + 1, MapHeight - i, 0);
		glTexCoord2f(s0, 1);
		glVertex3i(j, MapHeight - i, 0);
		glEnd();
	}

}

//Draw the background
void LevelEditor::Draw(){
	//Change to projection matrix (2D)
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glm::vec2 pos = calculateViewportPosition(windowPos);
	//gluOrtho2D(pos.x - HalfWindowWidth, pos.x + HalfWindowWidth, pos.y - WinowHeigth * (1.0f / 3.0f), pos.y + WinowHeigth * (2.0f / 3.0f));
	gluOrtho2D(pos.x - HalfWindowWidth, pos.x + HalfWindowWidth, 0, 20);// pos.y - WinowHeigth * (1.0f / 3.0f), pos.y + WinowHeigth * (2.0f / 3.0f));

	//Draw
	glMatrixMode(GL_MODELVIEW);


	//activate alpha testing 
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.1f);
	glColor4f(1, 1, 1, 1);

	//Draw the bg first
	glBindTexture(GL_TEXTURE_2D, bgID);
	DrawBG();

	//Draw every element of the map in the screen
	glBindTexture(GL_TEXTURE_2D, texID);
	glColor4f(1, 1, 1, 1);
	for (int i = 0; i < MapHeight; i++)
		for (int j = 0; j < MapWidth; j++)
		{
			DrawChar(i, j, scene[i][j]);
		}

	//Draw the object that the user is going to input
	int j = int(windowPos.x + mousePos.x - 10);
	int i = int(mousePos.y);
	DrawChar(i, j, actual);

	
}

//function to handle keyboard inputs
bool LevelEditor::keyboard(unsigned char c, int x, int y){
	// by now... esc resets!
	if (c == 27){
		return false;
	}
	switch (c){
	case 'x':
	case 'y':
	case 'z':
	case '1':
	case '2':
	case '3':
	case 'f':
	case 'p':
	case 'c':
		actual = c; //set the character as the actual character
		break;
	case 's':
	case 19:
	{
		//when saving the map
		if (controlPress){
			//save to file
			char * filename = "output.txt";
			FILE *f;
			fopen_s(&f, filename, "w");
			if (f == NULL)
			{
				printf("problem saving level \n");
				return 0;
			}

			int minW = -1;
			//found map size
			for (int i = 0; i < MapHeight; i++)
			{
				int lastblank = -1;
				for (int j = 0; j < MapWidth; j++)
				{
					if (scene[i][j] == ' ' && lastblank == -1){
						lastblank = j;
					}
					else if (scene[i][j] != ' ') lastblank = -1;
				}
				if (lastblank == -1) lastblank = MapWidth;
				minW = std::max(lastblank, minW);
			}

			fprintf(f, "%d %d\n", minW, 20);
			//write map to file
			for (int i = 0; i < MapHeight; i++)
			{
				for (int j = 0; j < minW; j++)
				{
					fprintf(f, "%c", scene[i][j]);
				}
				fprintf(f, "\n");
			}


			fclose(f);
		}
	}break;
	default:
		actual = ' ';
		break;
	}


	return true;
}

//function to handle keyboard inputs
void LevelEditor::keyboardUp(unsigned char c, int x, int y){

}

//function to handle keyboard inputs
void LevelEditor::specialKey(int key, int x, int y){
	switch (key)
	{
	case GLUT_KEY_CTRL_L:
		controlPress = true;
		break;
	}
}

//function to handle keyboard inputs
void LevelEditor::specialupKey(int key, int x, int y){
	switch (key)
	{
	case GLUT_KEY_CTRL_L:
		controlPress = false;
		break;
	}
}

//function to transform coordinates from screen to map coordiantes
glm::vec2 LevelEditor::transformCoordinates(glm::vec2 mousePos){
	int viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);

	float x = (mousePos.x) / viewport[2] * 2.0f - 1.0f; //to NDC
	float y = (mousePos.y) / viewport[3] * 2.0f - 1.0f; //to NDC;
	
	//To the moving windows size;
	x = x * 10 + 10;
	y = y * 10 + 10;
	
	
	return glm::vec2(x,y);
}

//function to handle mouse click
void LevelEditor::mouse(int button, int state, int x, int y){
	mousePos = transformCoordinates(glm::vec2(x, y)); //mouse in screen
	
	//if clicked with left button
	if (button == 0 && state == 1){
		// set the actual value in the matrix
		int j = int(windowPos.x + mousePos.x - 10);
		int i = int(mousePos.y);

		if (actual == 'f'){
			if(finalPos.x != -1 && finalPos.y != -1)
				scene[finalPos.y][finalPos.x] = ' ';
			finalPos = glm::ivec2(j, i);
		}

		if (actual == 'p'){
			if(playerPos.x != -1 && playerPos.y != -1)
				scene[playerPos.y][playerPos.x] = ' ';
			playerPos = glm::ivec2(j, i);
		}

		scene[i][j] = actual;
	}
	else if (button == 2){
		//if clicked with right button
		if (state == 0){
			//first click to drag the screen
			leftButtonPress = true;
			last = mousePos;
		}
		else{
			//No click anymore
			leftButtonPress = false;
		}
	}
}

//function to handle mouse movement
void LevelEditor::mouseMove(int x, int y){
	//transform coordiantes
	mousePos = transformCoordinates(glm::vec2(x, y));

	//Scroll only if the left button is played
	if (leftButtonPress){
		float difx = last.x - mousePos.x; //difference in coordinate

		windowPos.x += difx;
		windowPos = calculateViewportPosition(windowPos);

		last = mousePos; //update
	}
}

//function to handle mouse movement
void LevelEditor::mousePassiveMove(int x, int y){
	mousePos = transformCoordinates(glm::vec2(x, y));
}