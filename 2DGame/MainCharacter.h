#ifndef __MAINC_H__
#define __MAINC_H__

#include <stdio.h>
#include <string>
#include <vector>

#include "Definitions.h"
#include "SoundManager.h"

//How long does the walk last
#define FRAME_DURATION_WALK 3

//How long does the jump last
#define FRAME_DURATION_JUMP 6

//initial frame of the jump
#define FRAMES_JUMP 3

//initial frame of the walk
#define FRAMES_WALK 6

//where walk animation begins
#define FRAME_INITIAL_WALK 1

//wher jump animation begins
#define FRAME_INITIAL_JUMP 6

//where idle animation begins
#define FRAME_INITIAL_IDLE 0

//where dead animation begins
#define FRAME_INITIAL_DEAD 10

//Enum with possible palyer position
enum
{
	RIGHT_DIR, LEFT_DIR
};

//class that contains the player's character
class MainCharacter
{

private:
	//player's position
	glm::vec2 position;
	//moving direction of the player if jumping
	glm::vec2 jumpAcceleration;

	SoundManager *soundMgr; //Sound manager

	unsigned int jumpID1; //Junmp sound ID
	unsigned int jumpID2; //Junmp sound ID
	unsigned int fallID; //fall ID
	
	//texture of the player
	GLuint texID;

	//Animation frame
	int animationCounter;

	//last direction to render the player in the correct direction
	int lastDir;

	//some control booleans
	bool moveRight, moveLeft, isJumping, spacebarPress, dead;

public:
	//default constructor
	MainCharacter();

	//default destructor
	~MainCharacter();

	//Init funtion
	void Init();

	//function to display the player
	void Draw();

	//set of funtions to handle keyboard input
	void keyboard(unsigned char c, int x, int y);
	void keyboardUp(unsigned char c, int x, int y);
	void specialKey(int key, int x, int y);
	void specialupKey(int key, int x, int y);

	//method to get palyer's position
	glm::vec2 getPosition();

	//update the player's position depending of the movement
	glm::vec2 getUpdatePosition();

	//set players's positions. Boolean to set if the player has hit the floor
	void setPosition(glm::vec2 newPos, bool hitFloor);

	//function to make the player jump
	void Jump();

	//boolean to kill the player
	void setDead(bool value);
	
	
};

#endif