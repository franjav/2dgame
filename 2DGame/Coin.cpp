#include "Coin.h"


// constructor with position
Coin::Coin(const glm::vec2 initialPos){
	// copying the position
	position = initialPos;

	// initial animation frame... it is random!
	currentAnimationFrame = (rand() % (COIN_FRAMES_PER_IMAGE*MAX_COIN_ANIMATION_IMAGES));
}

// destructor, nothing to do
Coin::~Coin(){

}

// to draw the coin
void Coin::Draw(){
	// getting the image index
	int imageIndex = FIRST_COIN_ANIMATION_IMAGE + currentAnimationFrame / COIN_FRAMES_PER_IMAGE;
	glColor3f(1.0f, 1.0f, 1.0f);

	// s texture coordinates
	float s0 = imageIndex * 64.0f / 1024.0f;
	float s1 = (imageIndex+1) * 64.0f / 1024.0f;

	// drawing the coin
	glBegin(GL_QUADS);
		glTexCoord2d(s0, 0);
		glVertex3f(position.x, position.y, 0.0);
		glTexCoord2d(s1, 0);
		glVertex3f(position.x + 1.0f, position.y, 0.0);
		glTexCoord2d(s1, 1);
		glVertex3f(position.x + 1.0f, position.y + 1.0f, 0.0);
		glTexCoord2d(s0, 1);
		glVertex3f(position.x, position.y + 1.0f, 0.0);
	glEnd();

	// increasing the animation framek
	currentAnimationFrame++;
	if (currentAnimationFrame >= COIN_FRAMES_PER_IMAGE*MAX_COIN_ANIMATION_IMAGES)
		currentAnimationFrame = 0;
}


// intersecting player position with the coin
bool Coin::intersect(glm::vec2 playerPos){

	if (playerPos.x + 1.0F <= position.x ||
		playerPos.x >= position.x + 1.0F ||
		playerPos.y + 1.0F <= position.y ||
		playerPos.y >= position.y + 1.0F) return false;

	return true;
}