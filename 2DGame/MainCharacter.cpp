#include "MainCharacter.h"
#include "Texture.h"

//default constructor
MainCharacter::MainCharacter(){
	//Load audios
	soundMgr = SoundManager::createManager();
	soundMgr->loadAudio(".\\Sound\\jump1.ogg", &jumpID1, false);
	soundMgr->loadAudio(".\\Sound\\jump2.ogg", &jumpID2, false);
	soundMgr->loadAudio(".\\Sound\\fall.ogg", &fallID, false);

	//load the texture of the player
	texID = Texture::loadTexture("textures\\player.raw", 512, 64, 4);

	//init some variables
	Init();
}

//default destructor
MainCharacter::~MainCharacter(){
	// Force an audio source to stop playing.
	soundMgr->stopAudio(jumpID1);
	soundMgr->stopAudio(jumpID2);
	soundMgr->stopAudio(fallID);

	//Release audio
	soundMgr->releaseAudio(jumpID1);
	soundMgr->releaseAudio(jumpID2);
	soundMgr->releaseAudio(fallID);
}

//Init funtion
void MainCharacter::Init(){
	position = glm::vec2(5, 0);
	moveRight = moveLeft = isJumping = spacebarPress = false;
	animationCounter = 0;
	lastDir = RIGHT_DIR;
	dead = false;
}

//function to display the player
void MainCharacter::Draw(){
	glColor3f(1.0f, 1.0f, 1.0f);

	//enable alpha blending and the player's texture
	glBindTexture(GL_TEXTURE_2D, texID);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.1f);

	if (dead)
	{
		//dead
		int frame = FRAME_INITIAL_DEAD;
		float s0, s1;
		if (lastDir == RIGHT_DIR)	s0 = frame * 1.0f / 11.0f, s1 = (frame + 1) * 1.0f / 11.0f;
		else						s1 = frame * 1.0f / 11.0f, s0 = (frame + 1) * 1.0f / 11.0f;

		glBegin(GL_QUADS);
		glTexCoord2f(s0, 1);
		glVertex3f(position.x, position.y, 0.0);

		glTexCoord2f(s1, 1);
		glVertex3f(position.x + 1.0f, position.y, 0.0);

		glTexCoord2f(s1, 0);
		glVertex3f(position.x + 1.0f, position.y + 1.0f, 0.0);

		glTexCoord2f(s0, 0);
		glVertex3f(position.x, position.y + 1.0f, 0.0);
		glEnd();
	}
	else if (!moveRight && !moveLeft && !isJumping)
	{
		//idle
		float s0, s1;
		int frame = FRAME_INITIAL_IDLE;
		if (lastDir == RIGHT_DIR)	s0 = frame * 1.0f / 11.0f, s1 = (frame + 1.0f) / 11.0f;
		else						s1 = frame * 1.0f / 11.0f, s0 = (frame + 1.0f) / 11.0f;
		glBegin(GL_QUADS);
			glTexCoord2f(s0, 1);
			glVertex3f(position.x, position.y, 0.0);

			glTexCoord2f(s1, 1);
			glVertex3f(position.x + 1.0f, position.y, 0.0);

			glTexCoord2f(s1, 0);
			glVertex3f(position.x + 1.0f, position.y + 1.0f, 0.0);

			glTexCoord2f(s0, 0);
			glVertex3f(position.x, position.y + 1.0f, 0.0);
		glEnd();
	}
	else if (moveRight || lastDir == RIGHT_DIR)
	{
		int frame = 0;
		animationCounter++;
		//printf("%d ", animationCounter);
		if (!isJumping)
		{
			if (animationCounter >= FRAMES_WALK * FRAME_DURATION_WALK)
				animationCounter = 0;
			frame = FRAME_INITIAL_WALK + animationCounter / FRAME_DURATION_WALK;
		}
		else
		{
			if (animationCounter > FRAMES_JUMP * FRAME_DURATION_JUMP)
				animationCounter = FRAMES_JUMP * FRAME_DURATION_JUMP - 1;
			frame = FRAME_INITIAL_JUMP + animationCounter / FRAME_DURATION_JUMP;
		}
		glBegin(GL_QUADS);
			glTexCoord2f(frame * 1.0f / 11.0f, 1);
			glVertex3f(position.x, position.y, 0.0);

			glTexCoord2f((frame+1) * 1.0f / 11.0f, 1);
			glVertex3f(position.x + 1.0f, position.y, 0.0);

			glTexCoord2f((frame+1) * 1.0f / 11.0f, 0);
			glVertex3f(position.x + 1.0f, position.y + 1.0f, 0.0);

			glTexCoord2f(frame * 1.0f / 11.0f, 0);
			glVertex3f(position.x, position.y + 1.0f, 0.0);
		glEnd();
		
	}
	else if (moveLeft || lastDir == LEFT_DIR)
	{
		int frame = 0;
		animationCounter++;
		//printf("%d ", animationCounter);
		if (!isJumping)
		{
			if (animationCounter >= FRAMES_WALK * FRAME_DURATION_WALK)
				animationCounter = 0;
			frame = FRAME_INITIAL_WALK + animationCounter / FRAME_DURATION_WALK;
		}
		else
		{
			if (animationCounter > FRAMES_JUMP * FRAME_DURATION_JUMP)
				animationCounter = FRAMES_JUMP * FRAME_DURATION_JUMP - 1;
			frame = FRAME_INITIAL_JUMP + animationCounter / FRAME_DURATION_JUMP;
		}
		glBegin(GL_QUADS);
			glTexCoord2f((frame+1) * 1.0f / 11.0f, 1);
			glVertex3f(position.x, position.y, 0.0);

			glTexCoord2f(frame * 1.0f / 11.0f, 1);
			glVertex3f(position.x + 1.0f, position.y, 0.0);

			glTexCoord2f(frame * 1.0f / 11.0f, 0);
			glVertex3f(position.x + 1.0f, position.y + 1.0f, 0.0);

			glTexCoord2f((frame+1) * 1.0f / 11.0f, 0);
			glVertex3f(position.x, position.y + 1.0f, 0.0);
		glEnd();
	}
	
	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
}

//set of funtions to handle keyboard input
void MainCharacter::keyboard(unsigned char c, int x, int y){
	
	switch (c)
	{
	case 32:
		//jump if space bar
		spacebarPress = true;
		animationCounter = 0;
		if (!isJumping){
			Jump();
		}
		
		break;
	}
}

//set of funtions to handle keyboard input
void MainCharacter::keyboardUp(unsigned char c, int x, int y){
	
	switch (c)
	{
	case 32:
		spacebarPress = false;
		animationCounter = 0;
		break;
	}
}

//set of funtions to handle keyboard input
void MainCharacter::specialKey(int key, int x, int y){

	switch (key)
	{
	case GLUT_KEY_LEFT:
		moveLeft = true;
		
		lastDir = LEFT_DIR;
		break;
	case GLUT_KEY_RIGHT:
		moveRight = true;
		lastDir = RIGHT_DIR;
		break;
	}
}

//set of funtions to handle keyboard input
void MainCharacter::specialupKey(int key, int x, int y){
	switch (key)
	{
	case GLUT_KEY_LEFT:
		moveLeft = false;
		
		animationCounter = 0;
		break;
	case GLUT_KEY_RIGHT:
		moveRight = false;
		animationCounter = 0;
		break;
	}
}

//update the player's position depending of the movement
glm::vec2 MainCharacter::getUpdatePosition(){
	glm::vec2 newPos = position;
	if (moveRight) newPos.x += CHAR_VELOCITY;
	if (moveLeft) newPos.x -= CHAR_VELOCITY;

	newPos += jumpAcceleration;
	jumpAcceleration -= glm::vec2(0.0f, GRAVITY);

	if (jumpAcceleration.y < -0.3f) jumpAcceleration.y = -0.3f; //Do not fall very fast


	return newPos;
}

//method to get palyer's position
glm::vec2 MainCharacter::getPosition(){
	return position;
}

//set players's positions. Boolean to set if the player has hit the floor
void MainCharacter::setPosition(glm::vec2 newPos, bool hitFloor){
	position = newPos;
	
	if (hitFloor) jumpAcceleration = glm::vec2(0.0f, 0.0f);

	if (isJumping && hitFloor){
		soundMgr->playAudio(fallID, true);
		isJumping = false;
		if (spacebarPress) Jump();
	}
}

//function to make the player jump
void MainCharacter::Jump(){
	//Play jump audio
	if (rand() % 2 == 0){
		soundMgr->playAudio(jumpID1, true);
	}
	else{
		soundMgr->playAudio(jumpID2, true);
	}
	isJumping = true;
	jumpAcceleration = glm::vec2(0.0f, CHAR_JUMP_ACC);
}

//boolean to kill the player
void MainCharacter::setDead(bool value){
	dead = value;
}