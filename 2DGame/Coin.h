#ifndef __COIN_H__
#define __COIN_H__

#include <stdio.h>
#include <string>
#include <vector>
#include "Definitions.h"
#include "SoundManager.h"


// how many images has the animation
#define MAX_COIN_ANIMATION_IMAGES 7

// first image "index" inside the texture
#define FIRST_COIN_ANIMATION_IMAGE 3

// last image "index" inside the texture
#define LAST_COIN_ANIMATION_IMAGE (FIRST_COIN_ANIMATION_IMAGE + MAX_COIN_ANIMATION_IMAGES - 1)

// number of frames for each image
#define COIN_FRAMES_PER_IMAGE 5
 
// conin class
class Coin
{

private:
	// position of the coin inside the map
	glm::vec2 position;

	// animation frame
	int currentAnimationFrame;

public:

	// constructor with position
	Coin(const glm::vec2 initialPos);

	// destructor
	~Coin();

	// draw is called on every frame
	void Draw();

	//  given a player position, determine if the coin intersects the player
	bool intersect(glm::vec2 pos);
	
};

#endif