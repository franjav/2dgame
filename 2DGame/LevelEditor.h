#ifndef __LEVELEDITOR_H__
#define __LEVELEDITOR_H__

#include <stdio.h>
#include <string>
#include <vector>
#include <vector>

#include "Definitions.h"
#include "MainCharacter.h"
#include "SoundManager.h"
#include "Coin.h"
#include "Enemy.h"


//class that contains the functions for the level editor
class LevelEditor
{
private:
	//with and height of the map
	int MapWidth;
	int MapHeight;
	
	//Size of the window. Always 20x20
	int WindowWidth;
	int WinowHeigth;
	
	//Half window size
	int HalfWindowWidth;
	int HalfWindowHeight;

	// texture atlas for the objects of the scene
	GLuint texID, characterID, bgID, enemyTexID;

	//Array that stores all the scene
	char scene[20][500];
	
	//Actual input character
	char actual;

	//Booleans to handle left button and control key
	bool leftButtonPress, controlPress;

	//Some vectors to store auxiary positions
	glm::vec2 windowPos, mousePos, last;

	//Store final position and player position, to ensure that there are only one on all the map
	glm::ivec2 finalPos, playerPos;

	//Function to caltulate the current viewport depending on the player position
	glm::vec2 calculateViewportPosition(glm::vec2 playerPos);

	//function to transform coordinates from screen to map coordiantes
	glm::vec2 transformCoordinates(glm::vec2 mousePos);

	//Function to draw and object in the coordiantes i, j of the screen, depending of the char
	void DrawChar(int i, int j, char c);

	//Draw the background
	void DrawBG();
	


public:
	//level editor constructor
	LevelEditor();

	// default destructor
	~LevelEditor();
	
	//Init of the class
	void Init();
	
	//Function to draw all the map
	void Draw();
	
	//Set of functions to handle keyboard input for the level editor
	bool keyboard(unsigned char c, int x, int y);
	void keyboardUp(unsigned char c, int x, int y);
	void specialKey(int key, int x, int y);
	void specialupKey(int key, int x, int y);

	//Set of functions to handle mouse input for the level editor
	void mouse(int button, int state, int x, int y);
	void mouseMove(int x, int y);
	void mousePassiveMove(int x, int y);
};

#endif