#include "Scenario.h"
#include "Texture.h"


// default constructor
Scenario::Scenario(){
	player = new MainCharacter;
	soundMgr = SoundManager::createManager();

	MapWidth = 1024;
	MapHeight = 20;

	// logic size of the windows (for glOrtho)
	WindowWidth = 20;
	WinowHeigth = 20;

	// half width and height
	HalfWindowWidth = WindowWidth / 2;
	HalfWindowHeight = WinowHeigth / 2;

	//loading sounds
	soundMgr->loadAudio(".\\Sound\\caveMusic.ogg", &musicID, true);
	soundMgr->loadAudio(".\\Sound\\Coins.ogg", &coinsID, false);
	soundMgr->loadAudio(".\\Sound\\death1.ogg", &deathID1, false);
	soundMgr->loadAudio(".\\Sound\\death2.ogg", &deathID2, false);
	soundMgr->loadAudio(".\\Sound\\scream.ogg", &screamID, false);

	//Play audio
	soundMgr->playAudio(musicID, true);

	currentLevel = 0;
	scene = NULL;

	// loading textures
	texID = Texture::loadTexture("textures\\map_elements.raw", 1024, 64, 4);
	gameOverTexID= Texture::loadTexture("textures\\GameOver.raw", 512, 64, 4);
	numbersTexID = Texture::loadTexture("textures\\numbers.raw", 512, 64, 4);
	enemyTexID = Texture::loadTexture("textures\\bird.raw", 1024, 64, 4);
	bgID = Texture::loadTexture("textures\\background.raw", 1974, 406, 4);

	//glBindTexture(GL_TEXTURE_2D, 0);
	if (texID == 0 || gameOverTexID == 0 || numbersTexID==0  || enemyTexID==0)
	{
		exit(1);
	}

	status = PLAYING;
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	nLives = 3;
	score = 0;
	numLevels = 0;

}

Scenario::~Scenario(){
	// Force an audio source to stop playing.
	soundMgr->stopAudio(musicID);
	soundMgr->stopAudio(coinsID);
	soundMgr->stopAudio(deathID1);
	soundMgr->stopAudio(deathID2);
	soundMgr->stopAudio(screamID);

	//Release audio
	soundMgr->releaseAudio(musicID);
	soundMgr->releaseAudio(coinsID);
	soundMgr->stopAudio(deathID1);
	soundMgr->stopAudio(deathID2);
	soundMgr->stopAudio(screamID);

	// removing scene
	if (scene)
	{
		for (int i = 0; i < MapHeight; i++)
			delete[] scene[i];
		delete[] scene;
		scene = NULL;
	}
}

// computing viewport position from player pos
glm::vec2 Scenario::calculateViewportPosition(glm::vec2 playerPos){
	glm::vec2 pos = playerPos;

	if (playerPos.x - (HalfWindowWidth) <= 0.0f ) pos.x = float(HalfWindowWidth);
	if (playerPos.x + HalfWindowWidth >= MapWidth) pos.x = float(MapWidth - HalfWindowWidth);

	return pos;;
}

// draw score
void Scenario::drawScore()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, 30, 0, 30);

	// activating the texture
	glBindTexture(GL_TEXTURE_2D, numbersTexID);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_ALPHA_TEST);

	// do not draw transparent pixels
	glAlphaFunc(GL_GREATER, 0.6f);
	glColor4f(0.0f, 0.0f, 0.0f, 1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	int digits[10];
	int s = score;
	int length = 0;

	// computing number of digits
	while (s)
	{
		digits[length] = s % 10;
		length++;
		s  /= 10;
	}
	if (length == 0)
	{
		length = 1;
		digits[0] = 0;
	}


	// display digit by digit
	int x = 0;
	glBegin(GL_QUADS);
	for (int i = length - 1; i >= 0; i--, x++)
	{
		int d = digits[i];
		glTexCoord2f(d * 51.2f/512.0f, 1);
		glVertex3i(20 + x, 0, 0);

		glTexCoord2f((d + 1) * 51.2f / 512.0f, 1);
		glVertex3i(20 + x + 1, 0, 0);

		glTexCoord2f((d + 1) * 51.2f / 512.0f, 0);
		glVertex3i(20 + x + 1, 1, 0);

		glTexCoord2f(d * 51.2f / 512.0f, 0);
		glVertex3i(20 + x, 1, 0);
	}
	glEnd();
}


void Scenario::DrawBG(){
	int viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);

	//Draw clouds
	float s0 = 0.0f;
	float s1 = ((MAX_LVL_WIDTH / 20.0f) * viewport[2]) / 1971.0f;

	float t0 = 0.0f;
	float t1 = 1.0f;

	// quad with tex
	glBegin(GL_QUADS);
	glTexCoord2f(s0, t1);
	glVertex3i(0, 0, 0);
	glTexCoord2f(s1, t1);
	glVertex3i(MAX_LVL_WIDTH, 0, 0);
	glTexCoord2f(s1, t0);
	glVertex3i(MAX_LVL_WIDTH, MAX_LVL_HEIGHT, 0);
	glTexCoord2f(s0, t0);
	glVertex3i(0, MAX_LVL_HEIGHT, 0);
	glEnd();
}

void Scenario::Draw(){
	//Change to projection matrix (2D)
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glm::vec2 pos = calculateViewportPosition(player->getPosition());
	//gluOrtho2D(pos.x - HalfWindowWidth, pos.x + HalfWindowWidth, pos.y - WinowHeigth * (1.0f / 3.0f), pos.y + WinowHeigth * (2.0f / 3.0f));
	gluOrtho2D(pos.x - HalfWindowWidth, pos.x + HalfWindowWidth, 0, 20);// pos.y - WinowHeigth * (1.0f / 3.0f), pos.y + WinowHeigth * (2.0f / 3.0f));

	//Draw
	glMatrixMode(GL_MODELVIEW);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.1f);
	

	glBindTexture(GL_TEXTURE_2D, bgID);
	glColor4f(1, 1, 1, 1);
	DrawBG();
	

	glBindTexture(GL_TEXTURE_2D, texID);
	glColor4f(1, 1, 1, 1);
	for (int i = 0; i < MapHeight; i++)
		for (int j = 0; j < MapWidth; j++)
		{
			if (scene[i][j] == 'x' || scene[i][j] == 'y' || scene[i][j] == 'z')
			{
				float s0 = (int)(scene[i][j] - 'x') * 64.0f / 1024.0f;
				float s1 = (int)(scene[i][j] - 'x' + 1) * 64.0f / 1024.0f;
				// we draw a box
				glBegin(GL_QUADS);
				glTexCoord2f(s0, 0);
				glVertex3i(j, MapHeight-i, 0);
				glTexCoord2f(s1, 0);
				glVertex3i(j + 1, MapHeight-i, 0);
				glTexCoord2f(s1, 1);
				glVertex3i(j + 1, MapHeight-i-1, 0);
				glTexCoord2f(s0, 1);
				glVertex3i(j, MapHeight-i-1, 0);
				glEnd();
			}
			else if (scene[i][j] == 'f')
			{
				float s0 =  640.0f / 1024.0f;
				float s1 =  704.0f / 1024.0f;
				// we draw a box
				glBegin(GL_QUADS);
					glTexCoord2f(s0, 0);
					glVertex3i(j, MapHeight  - i, 0);
					glTexCoord2f(s1, 0);
					glVertex3i(j + 1, MapHeight  - i, 0);
					glTexCoord2f(s1, 1);
					glVertex3i(j + 1, MapHeight - i-1, 0);
					glTexCoord2f(s0, 1);
					glVertex3i(j, MapHeight - i-1, 0);

					s0 = 704.0f / 1024.0f;
					s1 = 768.0f / 1024.0f;

					glTexCoord2f(s0, 0);
					glVertex3i(j, MapHeight  - i+1, 0);
					glTexCoord2f(s1, 0);
					glVertex3i(j + 1, MapHeight - i+1, 0);
					glTexCoord2f(s1, 1);
					glVertex3i(j + 1, MapHeight - i  , 0);
					glTexCoord2f(s0, 1);
					glVertex3i(j, MapHeight - i, 0);
				glEnd();
			}
		}
	


	//Draw coins
	for (int i = 0; i < (int)coins.size(); ++i) coins[i]->Draw();

	glDisable(GL_ALPHA_TEST);
	

	glBindTexture(GL_TEXTURE_2D, enemyTexID);
	glEnable(GL_TEXTURE_2D);
	//Draw enemies
	for (int i = 0; i < (int)enemies.size(); ++i) enemies[i]->Draw();
	glDisable(GL_TEXTURE_2D);

	//Draw character
	player->Draw();

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, 2 * HalfWindowWidth, 0, 20);

	glBindTexture(GL_TEXTURE_2D, texID);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.1f);
	glColor4f(1, 1, 1, 1);

	if (nLives>0)
	{
		glBegin(GL_QUADS);
		for (int i = 0; i < nLives; i++)
		{
			float s0 = 12.0f*64.0f / 1024.0f;
			float s1 = 13.0f*64.0f / 1024.0f;
			glTexCoord2f(s0, 0);
			glVertex3f(i+0.0f, 0.5f, 0.0f);
			glTexCoord2f(s1, 0);
			glVertex3f(i + 0.5f, 0.5f, 0.0f);
			glTexCoord2f(s1, 1);
			glVertex3f(i + 0.5f, 0.0f, 0.0f);
			glTexCoord2f(s0, 1);
			glVertex3f(i + 0.0f, 0.0f, 0.0f);

		}
		glEnd();
	}
	if(locked) drawGameOver();
	drawScore();
}


void Scenario::keyboard(unsigned char c, int x, int y){
	// by now... esc resets!
	if (c == 27){
		locked = true;
		status = LOST;
	}else{

		//character keyboard
		if (!locked){
			if(c == 'R' || c == 'r'){
				//reload level
				score = scoreBeforeDie; //reset score
				int temp = --nLives;
				if (nLives == 0){
					locked = true; //if lives are equal to 0, then game over
				}
				else{
					loadScene(currentLevel);
					nLives = temp;
				}
			}else player->keyboard(c, x, y);
		}else{
			switch (c)
			{
			case 32:
				status = LOST; // unlock game over with space bar

				break;
			}
		}
	}
}

void Scenario::keyboardUp(unsigned char c, int x, int y){
	//character keyboard up
	if (!locked) player->keyboardUp(c, x, y);

}


void Scenario::specialKey(int key, int x, int y){
	//character special keyboard
	if (!locked) player->specialKey(key, x, y);

}

void Scenario::specialupKey(int key, int x, int y){
	//character special keyboard
	if (!locked) player->specialupKey(key, x, y);

}

// draw game over
void Scenario::drawGameOver()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, 100, 0, 100);

	//activating the corresponding texture
	glBindTexture(GL_TEXTURE_2D, gameOverTexID);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.1f);
	glColor4f(1, 1, 1, 1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// quad with texture
	glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex3f(25, 60, 0);
		glTexCoord2f(1, 0);
		glVertex3f(75, 60, 0);
		glTexCoord2f(1, 1);
		glVertex3f(75, 40, 0);
		glTexCoord2f(0, 1);
		glVertex3f(25, 40, 0);
	glEnd();
}


gameStatus Scenario::update(){
	//if gameover don't do interactions
	if (!locked){
		
		switch (status){
			// if character dying wait some seconds
			case DYING:{

				//decide moving direction
				int timeSinceStart = glutGet(GLUT_ELAPSED_TIME);
				int deltaTime = timeSinceStart - oldTimeSinceStart;

				if (deltaTime > 500)
				{
					oldTimeSinceStart = timeSinceStart;
					score = scoreBeforeDie; //reset score
					int temp = --nLives;
					if (nLives == 0){
						locked = true; //if lives are equal to 0, then game over
					}
					else{
						loadScene(currentLevel);
						nLives = temp;
					}
				}

			}break;
			case NEXTLEVEL:{
				if (currentLevel == numLevels) return WIN;
				score += 20; //20 points for advancing a level
				scoreBeforeDie = score; //save score when you pass level
				loadScene(++currentLevel); //Do somthing
				status = PLAYING;
			}
			case PLAYING:{
				gameStatus s = IntersectWithScenario();
				if (s == NEXTLEVEL) status = NEXTLEVEL;
				else if (s == DYING){
					oldTimeSinceStart = glutGet(GLUT_ELAPSED_TIME);
					player->setDead(true);
					status = DYING;
				}else{
					s = IntersectWithEnemy();
					if (s == DYING){
						oldTimeSinceStart = glutGet(GLUT_ELAPSED_TIME);
						player->setDead(true);
						status = DYING;
					}else IntersectWithCoin();
				}
			}break;
			default:
				break;
		}
	}

	

	return status;
}

gameStatus Scenario::IntersectWithScenario(){

	//Check intersection with Scenario
	glm::vec2 prevPos = player->getPosition();
	glm::vec2 newPos = player->getUpdatePosition();
	glm::vec2 finalPos = newPos;
	bool hitFloor = false;

	//For now, only intersetion with floor and walls, you die!
	if (newPos.y < 0.0f || (newPos.y >= MapHeight) || newPos.x < 0.0f || newPos.x >= MapWidth -1){
		//you die
		soundMgr->playAudio(screamID, true);
		return DYING;
	}


	//Intersect with boxes only moving in X for now
	if (checkInMap(finalPos.x, prevPos.y)){
		finalPos.x = prevPos.x;
	}

	if (checkInMap(finalPos.x, finalPos.y)){
		//check if it hits ground
		if (finalPos.y < prevPos.y) hitFloor = true;
		finalPos.y = prevPos.y;
	}

	//Set the new calculated position
	player->setPosition(finalPos, hitFloor);

	//If the player gets to the end
	if (checkInMapWin()) return NEXTLEVEL;


	std::vector<int> deadEnemies;

	//Do the same for all monsters, they could hit something
	for (int i = 0; i < (int)enemies.size(); ++i){
		//Check intersection with Scenario
		glm::vec2 prevPos = enemies[i]->getPosition();
		glm::vec2 newPos = enemies[i]->getUpdatePosition(player->getPosition());
		glm::vec2 finalPos = newPos;
		bool hitFloor = false;

		//For now, only intersetion with floor and walls
		if (newPos.y < 0.0f || (newPos.y >= MapHeight) || newPos.x < 0.0f || newPos.x >= MapWidth - 1){
			//enmy dies
			deadEnemies.push_back(i);
		}

		//Intersect with boxes only moving in X for now
		if (checkInMap(finalPos.x, prevPos.y)){
			finalPos.x = prevPos.x;
		}

		if (checkInMap(finalPos.x, finalPos.y)){
			//check if it hits ground
			if (finalPos.y < prevPos.y) hitFloor = true;
			finalPos.y = prevPos.y;
		}

		//Set the new calculated position
		enemies[i]->setPosition(finalPos, hitFloor);
	}

	//Kill enemies outside map
	for (int i = deadEnemies.size() - 1; i >= 0; --i){
		enemies.erase(enemies.begin() + deadEnemies[i]);
		//Play death audio
		if (rand() % 2 == 0){
			soundMgr->playAudio(deathID1, true);
		}
		else{
			soundMgr->playAudio(deathID2, true);
		}
	}
	deadEnemies.clear();

	return PLAYING;
}

bool Scenario::checkInMap(float x, float y){
	glm::ivec2 pos1 = glm::ivec2(MapHeight - 1 - int(y + 0.05f), int(x + 0.05f)); //lower left
	glm::ivec2 pos2 = glm::ivec2(MapHeight - 1 - int(y + 0.95f), int(x + 0.95f)); //upper right
	glm::ivec2 pos3 = glm::ivec2(MapHeight - 1 - int(y + 0.05f), int(x + 0.95f)); //lower right
	glm::ivec2 pos4 = glm::ivec2(MapHeight - 1 - int(y + 0.95f), int(x + 0.05f)); //upper left

	//check if the player hits the wall
	return (scene[pos1.x][pos1.y] == 'x' ||
		scene[pos1.x][pos1.y] == 'y' ||
		scene[pos1.x][pos1.y] == 'z' ||
		scene[pos2.x][pos2.y] == 'x' ||
		scene[pos2.x][pos2.y] == 'y' ||
		scene[pos2.x][pos2.y] == 'z' ||
		scene[pos3.x][pos3.y] == 'x' ||
		scene[pos3.x][pos3.y] == 'y' ||
		scene[pos3.x][pos3.y] == 'z' ||
		scene[pos4.x][pos4.y] == 'x' ||
		scene[pos4.x][pos4.y] == 'y' ||
		scene[pos4.x][pos4.y] == 'z');
}

bool Scenario::checkInMapWin(){
	glm::ivec2 pos1 = glm::ivec2(MapHeight - 1 - int(player->getPosition().y + 0.05f), int(player->getPosition().x + 0.05f)); //lower left
	glm::ivec2 pos2 = glm::ivec2(MapHeight - 1 - int(player->getPosition().y + 0.95f), int(player->getPosition().x + 0.95f)); //upper right
	glm::ivec2 pos3 = glm::ivec2(MapHeight - 1 - int(player->getPosition().y + 0.05f), int(player->getPosition().x + 0.95f)); //lower right
	glm::ivec2 pos4 = glm::ivec2(MapHeight - 1 - int(player->getPosition().y + 0.95f), int(player->getPosition().x + 0.05f)); //upper left

	//check if you are at the final position and if the player has collected all coins
	return (coins.size() == 0 && (
			scene[pos1.x][pos1.y] == 'f' ||
			scene[pos2.x][pos2.y] == 'f' ||
			scene[pos3.x][pos3.y] == 'f' ||
			scene[pos4.x][pos4.y] == 'f') );
}



gameStatus Scenario::IntersectWithEnemy(){
	//check intersection with the enemies
	for (int i = 0; i < (int)enemies.size(); ++i){
		IntersectionEnemyType type = enemies[i]->intersect(player->getPosition());
		if (type == KILL_PLAYER){
			//If you hit an enemy, you die!
			soundMgr->playAudio(screamID, true);

			return DYING;
			break;
		}
		else if (type == KILL_ENEMY){
			//unless you jump over him!!!
			//Play death audio
			if (rand() % 2 == 0){
				soundMgr->playAudio(deathID1, true);
			}
			else{
				soundMgr->playAudio(deathID2, true);
			}
			score += 5; //5 points to kill enemy
			enemies.erase(enemies.begin() + i);
			player->Jump(); //Jump again in the head of the enemy
			break;
		}
	}
	return PLAYING;
}

void Scenario::IntersectWithCoin(){
	//check intersection with coins
	for (int i = 0; i < (int)coins.size(); ++i){
		if (coins[i]->intersect(player->getPosition())){
			//Play audio
			soundMgr->playAudio(coinsID, true);
			coins.erase(coins.begin() + i);
			score += 1; //1 point to take coin
			break;
		}
	}
}

// load the scene with index "index"
bool Scenario::loadScene(int index)
{
	// if the level is the first one, initialize other stuff
	if (index == 1){
		nLives = 3;
		score = 0;
		scoreBeforeDie = 0;
	}

	status = PLAYING;
	locked = false;

	// delete old scene map
	if (scene)
	{
		for (int i = 0; i < MapHeight; i++)
			delete[] scene[i];
		delete[] scene;
		scene = NULL;
	}

	currentLevel = index;
	player->Init();
	coins.clear();
	enemies.clear();

	// loading...
	char filename[80];
	sprintf_s(filename, "levels\\level%d.txt", currentLevel);
	FILE *f;
	fopen_s(&f,filename, "rt");
	if (f == NULL)
	{
		printf("Error opening file %s\n", filename);
		return false;
	}
	if (fscanf_s(f, "%d %d\n", &MapWidth, &MapHeight) != 2)
	{
		fclose(f);
		printf("Error loading file %s\n", filename);
		return false;
	}
	//printf("level is %d x %d\n", MapWidth, MapHeight);
	scene = new char*[MapHeight];
	for (int i = 0; i < MapHeight; i++)
	{
		scene[i] = new char[MapWidth];
		for (int j = 0; j < MapWidth; j++)
		{
			if (fscanf_s(f, "%c", &scene[i][j]) != 1)
			{
				fclose(f);
				printf("Error loading file %s\n", filename);
				return false;
			}
			//printf("%c", scene[i][j]);
			switch (scene[i][j])
			{
				// 3 types of enemies
				case '1': scene[i][j] = ' '; enemies.push_back(new Enemy(glm::vec2(j, MapHeight - 1 - i), ENEMY_LEVEL1)); break;
				case '2': scene[i][j] = ' '; enemies.push_back(new Enemy(glm::vec2(j, MapHeight - 1 - i), ENEMY_LEVEL2)); break;
				case '3': scene[i][j] = ' '; enemies.push_back(new Enemy(glm::vec2(j, MapHeight - 1 - i), ENEMY_LEVEL3)); break;
				case 'x':	// three types of boxes
				case 'y':
				case 'z':	break; 
				case 'c': scene[i][j] = ' '; coins.push_back(new Coin(glm::vec2(j, MapHeight - 1 - i))); break;
				case 'f': break; // final position
				case 'p': scene[i][j] = ' '; player->setPosition(glm::vec2(j, MapHeight - 1 - i), false); break; // player position
			}
		}
		fscanf_s(f, "\n");

	}

	fclose(f);
	return true;
}


bool Scenario::checkNumLevels(){
	//calculate the number of level files ...
	numLevels = 0;

	bool end = false;
	int i = 1;
	while (!end){
		char filename[80];
		sprintf_s(filename, "levels\\level%d.txt", i);
		FILE *f;
		fopen_s(&f, filename, "rt");
		if (f != NULL)
		{
			fclose(f);
			++i;
			++numLevels;
		}
		else{
			end = true;
		}
	}

	return numLevels > 0;
}