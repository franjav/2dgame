#ifndef __ENEMY_H__
#define __ENEMY_H__

#include <stdio.h>
#include <string>
#include <vector>

#include "Definitions.h"
#include "SoundManager.h"


//Posible enemy looking direction (left, right)
enum EnemyDir{
	ENEMY_RIGHT_DIR, ENEMY_LEFT_DIR
};

//Type of intersection of player with the enemy
enum IntersectionEnemyType{ NONE, KILL_PLAYER, KILL_ENEMY };

//AI level of the enemy
enum EnemyType{ ENEMY_LEVEL1 = 1, ENEMY_LEVEL2 = 2, ENEMY_LEVEL3 = 3 };

//Enemy class
class Enemy
{

private:
	//Position of the enemy in the map
	glm::vec2 position;

	//Moving velocity in the Y axis
	glm::vec2 jumpAcceleration;

	//Moving velocity in the X axis
	glm::vec2 movingDirection;

	//If the player is jumping
	bool jump;

	//Level of the enemy
	EnemyType enemyType;

	//variable with the last enemy looking direction
	EnemyDir lastDir;

	//Ellapsed time (for AI propouse)
	int oldTimeSinceStart; 

	//For animation propouse
	int animationCounter;

public:
	
	//Constructor of the enemy with initial position and type of the enemy's AI
	Enemy(const glm::vec2 initialPos, EnemyType enemyType);

	//default destructor
	~Enemy();

	//Function to draw the enemy
	void Draw();

	//Calculate intersection of the player with the enemy
	IntersectionEnemyType intersect(glm::vec2 playerPos);

	//Get method
	glm::vec2 getPosition();

	//Update enemy's position. Depending of the enemy's AI, the position will depend of the player's position
	glm::vec2 getUpdatePosition(glm::vec2 playerPos);

	//Set position of the enemy. A boolean indicates if the enemy has fallen into the floor
	void setPosition(glm::vec2 newPos, bool hitFloor);
	
};

#endif