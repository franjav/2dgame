#include "Definitions.h"
#include "SoundManager.h"
#include "Scenario.h"
#include "GameMenu.h"
#include "LevelEditor.h"
#include "texture.h"

//sound manager to contain all the sound
SoundManager* soundMgr = SoundManager::createManager();

//mode of the game in a specific moment
enum Mode{ MENU, GAME, EDITOR, HELP };
Mode mode;

//objects to handle menu, levels, and level editor
Scenario *level;
GameMenu *menu;
LevelEditor *editor;

//Opengl texture
GLuint helpTexID=0;

void resize(int width, int height) {
	// we ignore the params and do:
	glViewport(0, 0, width, height);
	//glutReshapeWindow(WINDOW_WIDTH, WINDOW_HEIGTH);
}

void InitAudio(){
	//Init audio must be the first thing to do 
	soundMgr->init();
}

//Idle function to ensure redisplay
void Idle(){
	switch (mode){
		case GAME:{
			//change mode if the player has win or lose
			gameStatus s = level->update();
			if (s == LOST || s == WIN) mode = MENU;
			break;
		}
	}
	glutPostRedisplay();
}

//function to handle special key input up
void specialupKey(int key, int x, int y){
	switch (mode){
	case MENU:
		break;
	case GAME:
		level->specialupKey(key, x, y);
		break;
	case EDITOR:
		editor->specialupKey(key, x, y);
		break;
	case HELP:
		mode = MENU;
		break;
	}

}

//function to handle special key input
void specilaKey(int key, int x, int y)
{
	switch (mode){
	case MENU:
		break;
	case GAME:
		level->specialKey(key, x, y);
		break;
	case EDITOR:
		editor->specialKey(key, x, y);
		break;
	}
	
}

//function to handle keyboard input
void keyboard(unsigned char c, int x, int y){
	

	switch (mode){
	case MENU:
		// by now... esc exits!
		if (c == 27)
			exit(1);
		break;
	case GAME:
		level->keyboard(c,x,y);
		break;
	case EDITOR:
		bool stay = editor->keyboard(c, x, y);
		if (!stay) mode = MENU;
		break;
	}
}

//function to handle key up
void keyboardUp(unsigned char c, int x, int y) {
	switch (mode) {
	case MENU:
		break;
	case GAME:
		level->keyboardUp(c, x, y);
		break;
	case EDITOR:
		editor->keyboardUp(c, x, y);
		break;
	case HELP:
		mode = MENU;
		break;
	}
}

//function to handle mouse click
void mouse(int button, int state, int x, int y){
	switch (mode){
		case MENU: {
			if (button == 0 && state == 1)
			{
				int opc = menu->mouseEvent(button, state, x, y);
				if (opc == 1)
				{
					// turn on, or turn off the sound

					soundMgr->enableSound(menu->isSoundOn());
				}
				else if (opc == 0)
				{
					// new game
					mode = GAME;
					if(level->checkNumLevels())	level->loadScene(1);
					else mode = MENU;
				}
				else if (opc == 2)
				{
					// level editor
					mode = EDITOR;
					editor->Init();
				}
				else if (opc == 3)
				{
					// show help
					mode = HELP;
				}
				else if (opc == 4)
				{
					exit(1);
				}
			}
			break;
		}
		case GAME:
			break;
		case HELP:
			if (button == 0 && state == 1)
				mode = MENU;
			break;
		case EDITOR:
			editor->mouse(button, state, x, y);
			break;
	}
}

//function to handle mouse motion
void mouseMove(int x, int y){
	switch (mode) {
	case MENU: 
		menu->mouseMove(x, y);
		break;
	case GAME:
		break;
	case EDITOR:
		editor->mouseMove(x, y);
		break;
	}
}

//function to handle mouse motion
void mousePassiveMove(int x, int y){
	switch (mode) {
	case MENU:
		menu->mousePassiveMove(x, y);
		break;
	case GAME:
		break;
	case EDITOR:
		editor->mousePassiveMove(x, y);
		break;
	}
}

//function to draw the help
void drawHelp()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, 100, 0, 100);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//draw a quad with the texture help
	glBindTexture(GL_TEXTURE_2D, helpTexID);
	glEnable(GL_TEXTURE_2D);
	glColor4f(1, 1, 1, 1);

	glBegin(GL_QUADS);
		glTexCoord2f(0, 1);
		glVertex3f(0, 0, 0);

		glTexCoord2f(1, 1);
		glVertex3f(100, 0, 0);

		glTexCoord2f(1, 0);
		glVertex3f(100, 100, 0);

		glTexCoord2f(0, 0);
		glVertex3f(0, 100, 0);

	glEnd();

}

//main displya function
void display(void)
{
	glClearColor(1, 1, 1, 1);
	glClear(GL_COLOR_BUFFER_BIT);

	switch (mode){
	case MENU:
		//display menu
		menu->display();

		break;
	case GAME:
		//display level
		level->Draw();

		break;
	case EDITOR:
		//display level editor
		editor->Draw();
		break;
	case HELP:
		//display help
		drawHelp();
		break;
	}

	glutSwapBuffers();
	Sleep(1000 / 60);
}

//Function to init each component
void Init(){

	mode = MENU; // GAME;
	InitAudio();
	menu = new GameMenu(); //create menu
	if (!menu->Init())
	{
		exit(1);
	}
	level = new Scenario(); //create levels
	editor = new LevelEditor(); //create level editor

	//load help texture
	helpTexID = Texture::loadTexture("textures\\help.raw", 512, 512, 3);
}

void Finish(){
	//Destoy dynamic objects
	delete level;
	delete editor;
	delete menu;
	delete soundMgr;
}

int main(int argc, char** argv)
{
	

	//set flut window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(1024, 768);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("Game");

	//Init all
	Init();

	//set glut functions
	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutMouseFunc(mouse);
	glutMotionFunc(mouseMove);
	glutPassiveMotionFunc(mousePassiveMove);
	glutKeyboardFunc(keyboard);
	glutKeyboardUpFunc(keyboardUp);
	glutSpecialFunc(specilaKey);
	glutSpecialUpFunc(specialupKey);
	glutFullScreen();
	glutIdleFunc(Idle);
	glutMainLoop();
	return 0;
}

