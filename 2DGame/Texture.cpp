#include "Texture.h"


// load the texture (pixels) of w=width and h=height into GPU. components should be 3 (RGB) or 4 (RGBA)
//texture is always in raw format
GLuint Texture::loadTexture(const char *filename, int w, int h, int components)
{
	//Open the file
	FILE *f;
	fopen_s(&f,filename, "rb");
	if (f == NULL)
	{
		printf("File not found: %s\n", filename);
		return 0;
	}

	//Allocated enough memory to hold the texture in CPU
	int nBytes = w * h * components;
	unsigned char *p = (unsigned char *)malloc(nBytes);

	//Read texture information
	fread(p, nBytes, 1, f);
	fclose(f);

	// generating a texture identifier
	GLuint textureID;
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);

	// linear texture filter
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// when the texture is outside [0,1], we just repeat the texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// modulate means combine the image with the current color
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	// create mipmaps for better filtering
	if (components == 3)
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, p);
	else
		gluBuild2DMipmaps(GL_TEXTURE_2D, 4, w, h, GL_RGBA, GL_UNSIGNED_BYTE, p);
	free(p);
	// return the created texture id 
	return textureID;
}