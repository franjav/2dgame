#include "Enemy.h"


//Constructor of the enemy with initial position and type of the enemy's AI
Enemy::Enemy(const glm::vec2 initialPos, EnemyType enemyType){
	position = initialPos;

	jump = false;

	jumpAcceleration -= glm::vec2(0.0f, GRAVITY);
	this->enemyType = enemyType;
	oldTimeSinceStart = 0;
	movingDirection = glm::vec2(0.0f);
	animationCounter = 0;
	lastDir = ENEMY_RIGHT_DIR;
}

//default destructure
Enemy::~Enemy(){

}

//Function to draw the enemy
void Enemy::Draw(){

	//Calculate the frame to be renderer from the texture
	animationCounter++;
	int factor = 4;
	if (animationCounter == 14 * factor)
		animationCounter = 0;
	int frame = animationCounter / factor;

	//Activate alpha test (just in case)
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.1f);

	float s0, s1;

	//Decide direction of the texture
	if (lastDir == ENEMY_RIGHT_DIR) s0 = frame * 1.0f / 14.0f, s1 = (frame + 1) * 1.0f / 14.0f;
	if (lastDir == ENEMY_LEFT_DIR) s0 = (frame + 1) * 1.0f / 14.0f, s1 = frame * 1.0f / 14.0f;

	//Draw enemy
	glBegin(GL_QUADS);
		glTexCoord2f(s0, 1.0f);
		glVertex3f(position.x, position.y, 0.0);
		glTexCoord2f(s1, 1.0f);
		glVertex3f(position.x + 1.0f, position.y, 0.0);
		glTexCoord2f(s1, 0.0f);
		glVertex3f(position.x + 1.0f, position.y + 1.0f, 0.0);
		glTexCoord2f(s0, 0.0f);
		glVertex3f(position.x, position.y + 1.0f, 0.0);
	glEnd();
}

//Update enemy's position. Depending of the enemy's AI, the position will depend of the player's position
glm::vec2 Enemy::getUpdatePosition(glm::vec2 playerPos){
	glm::vec2 newPos = position;

	if (enemyType == ENEMY_LEVEL1){
		//Move direction
		newPos += movingDirection;

		//decide moving direction
		int timeSinceStart = glutGet(GLUT_ELAPSED_TIME);
		int deltaTime = timeSinceStart - oldTimeSinceStart;

		if (deltaTime > 500)
		{ 
			oldTimeSinceStart = timeSinceStart;
			//Decide direction
			switch (rand() % 3){
			case(0) :
				lastDir = ENEMY_RIGHT_DIR;
				movingDirection = glm::vec2(ENEMY_VELOCITY, 0.0f);
				break;
			case(1) :
				lastDir = ENEMY_LEFT_DIR;
				movingDirection = glm::vec2(-ENEMY_VELOCITY, 0.0f);
				break;
			case(2) :
				movingDirection = glm::vec2(0.0f, 0.0f);
				break;
			}
		}
	}

	//Basic IA enemy. Follow the player if the player is near enough
	if (enemyType == ENEMY_LEVEL2 || enemyType == ENEMY_LEVEL3){
		if (glm::distance(playerPos, position) < 15.0f){
			if (playerPos.x < position.x + 0.3f){
				lastDir = ENEMY_LEFT_DIR;
				newPos.x -= ENEMY_VELOCITY;
			}
			if (playerPos.x > position.x - 0.3f){
				lastDir = ENEMY_RIGHT_DIR;
				newPos.x += ENEMY_VELOCITY;
			}
		}
	}

	if (enemyType == ENEMY_LEVEL3){
		//Jump now and then
		if (!jump && rand() % 50 == 0){
			jump = true;
			jumpAcceleration = glm::vec2(0.0f, ENEMY_JUMP_ACC);
		}
	}
	
	//Jump or fall
	newPos += jumpAcceleration;
	jumpAcceleration -= glm::vec2(0.0f, GRAVITY);
	if (jumpAcceleration.y < -0.3f) jumpAcceleration.y = -0.3f; //Do not fall very fast
	

	return newPos;
}

//Calculate intersection of the player with the enemy
IntersectionEnemyType Enemy::intersect(glm::vec2 playerPos){

	//There is no intersection
	if (playerPos.x + 1.0F <= position.x ||
		playerPos.x >= position.x + 1.0F ||
		playerPos.y + 1.0F <= position.y ||
		playerPos.y >= position.y + 1.0F) return NONE;

	//See if the player intersects from abode
	if (playerPos.y > position.y + 0.3f) return KILL_ENEMY;

	//player intersects with enemy
	return KILL_PLAYER;
}

//Get method
glm::vec2 Enemy::getPosition(){
	return position;
}

//Set position of the enemy. A boolean indicates if the enemy has fallen into the floor
void Enemy::setPosition(glm::vec2 newPos, bool hitFloor){
	position = newPos;
	if (hitFloor) jump = false; //If it hits the floor, it can jump again
	if (hitFloor) jumpAcceleration = glm::vec2(0.0f, 0.0f); //there is no up velocity
}