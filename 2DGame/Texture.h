#ifndef TEXTURE_H
#define TEXTURE_H

#include "Definitions.h"

//A class with only a statuic function to load a texture
class Texture{
public:
	//function to load a texture
	static GLuint loadTexture(const char *filename, int w, int h, int components);

};


#endif