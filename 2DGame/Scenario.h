#ifndef __SCENARIO_H__
#define __SCENARIO_H__

#include <stdio.h>
#include <string>
#include <vector>
#include <vector>

#include "Definitions.h"
#include "MainCharacter.h"
#include "SoundManager.h"
#include "Coin.h"
#include "Enemy.h"



// all possibilities for the game
enum gameStatus{ WIN, PLAYING, LOST, NEXTLEVEL, DYING};


// scenario class
class Scenario
{
private:
	// maximum size of the map
	int MapWidth;
	int MapHeight;

	// logic size of the windows (for glOrtho)
	int WindowWidth;
	int WinowHeigth;

	// half width and height
	int HalfWindowWidth;
	int HalfWindowHeight;

	// current level index
	int currentLevel;

	// number of levels
	int numLevels;

	// number of lives (starts with 3)
	int nLives;

	// player score
	int score;

	// score before die... just in case the player is eaten
	int scoreBeforeDie;

	// time
	int oldTimeSinceStart;
	
	// to avoid movements
	bool locked; //lock movements

	// current game status
	gameStatus status;

	// texture atlas for the object of the scene
	GLuint texID;

	// game over texture ID
	GLuint gameOverTexID;

	// texture ID for the numbers (used for score)
	GLuint numbersTexID;

	// texture ID for enemy
	GLuint enemyTexID;

	// texture ID for the background
	GLuint bgID;

	// scene map
	char ** scene;

	//the player
	MainCharacter *player; 

	//Sound manager
	SoundManager *soundMgr; 

	//Music ID
	unsigned int musicID; 

	//Coins grab ID
	unsigned int coinsID; 

	//Enamy Death sound ID
	unsigned int deathID1;

	//Enamy Death sound ID2
	unsigned int deathID2;

	//Enamy Death sound ID2
	unsigned int screamID; 

	// All coins
	std::vector<Coin *> coins; 

	// All enemies
	std::vector<Enemy *> enemies;

	// given the player position, it computes the viewport position
	glm::vec2 calculateViewportPosition(glm::vec2 playerPos);

	// intersect the player with the scenario (including enemies) 
	gameStatus IntersectWithScenario();

	// intersect player with enemies
	gameStatus IntersectWithEnemy();

	// intersect player with coin
	void IntersectWithCoin();

	// check collision with the map walls
	bool checkInMap(float x, float y);

	// check is player collides with the exit door (pass level)
	bool checkInMapWin();

	// draw a sky background
	void DrawBG();

public:

	// default constructor
	Scenario();

	// destructor
	~Scenario();

	// draw the frame
	void Draw();

	// draw a quad with game over
	void drawGameOver();

	// draw the score at the lower bottom area
	void drawScore();

	// keyboard down key call back
	void keyboard(unsigned char c, int x, int y);
	
	// keyboard up key call back
	void keyboardUp(unsigned char c, int x, int y);
	
	// special key call back
	void specialKey(int key, int x, int y);
	
	// special key up callback
	void specialupKey(int key, int x, int y);
	
	// update the game status
	gameStatus update();

	// load scene with index "index"
	bool loadScene(int index);

	// determine the number of available levels
	bool checkNumLevels();
};

#endif